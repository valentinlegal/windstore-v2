-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: 
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `symfony`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `symfony` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `symfony`;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'Bureautique','5c7953fe1f498938564140.png','2019-03-02 10:43:59'),(2,'Multimédia','5c7930c96b6a1355990786.png','2019-03-01 14:16:57'),(3,'Loisirs','5c7930c044eac371552061.png','2019-03-01 14:16:48'),(4,'Internet','5c792fbfc6b05705450449.png','2019-03-01 14:12:31'),(5,'Développement','5c7930fac71e4203791658.png','2019-03-01 14:17:46'),(6,'Sécurité','5c79310650ed5011621978.png','2019-03-01 14:17:58'),(7,'Utilitaires','5c7953f633e37448780454.png','2019-03-01 16:47:02');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `developpeur`
--

DROP TABLE IF EXISTS `developpeur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developpeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `developpeur`
--

LOCK TABLES `developpeur` WRITE;
/*!40000 ALTER TABLE `developpeur` DISABLE KEYS */;
INSERT INTO `developpeur` VALUES (1,'Mozilla Foundation','https://www.mozilla.org'),(2,'VideoLAN','https://www.videolan.org'),(3,'AVAST Software','https://www.avast.com'),(4,'Piriform','https://www.piriform.com'),(5,'The Document Foundation','https://www.documentfoundation.org'),(6,'GeoGebra','https://www.geogebra.org'),(7,'Adobe Inc.','https://www.adobe.com');
/*!40000 ALTER TABLE `developpeur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution`
--

DROP TABLE IF EXISTS `distribution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution`
--

LOCK TABLES `distribution` WRITE;
/*!40000 ALTER TABLE `distribution` DISABLE KEYS */;
INSERT INTO `distribution` VALUES (1,'Gratuit'),(2,'Freemium');
/*!40000 ALTER TABLE `distribution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licence`
--

DROP TABLE IF EXISTS `licence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licence`
--

LOCK TABLES `licence` WRITE;
/*!40000 ALTER TABLE `licence` DISABLE KEYS */;
INSERT INTO `licence` VALUES (1,'MPL-2.0'),(2,'LGPL-2.1+'),(3,'Propriétaire'),(4,'Mozilla Public License'),(5,'GNU GPL'),(6,'CC-BY-NC'),(7,'Licence MIT');
/*!40000 ALTER TABLE `licence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logiciel`
--

DROP TABLE IF EXISTS `logiciel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logiciel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `developpeur_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `distribution_id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taille` double NOT NULL,
  `lien_dl32` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien_dl64` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2C50669C84E66085` (`developpeur_id`),
  KEY `IDX_2C50669CBCF5E72D` (`categorie_id`),
  KEY `IDX_2C50669C6EB6DDB5` (`distribution_id`),
  CONSTRAINT `FK_2C50669C6EB6DDB5` FOREIGN KEY (`distribution_id`) REFERENCES `distribution` (`id`),
  CONSTRAINT `FK_2C50669C84E66085` FOREIGN KEY (`developpeur_id`) REFERENCES `developpeur` (`id`),
  CONSTRAINT `FK_2C50669CBCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logiciel`
--

LOCK TABLES `logiciel` WRITE;
/*!40000 ALTER TABLE `logiciel` DISABLE KEYS */;
INSERT INTO `logiciel` VALUES (1,1,4,1,'Mozilla Firefox','Mozilla Firefox est un navigateur web libre proposé par la Fondation Mozilla. Firefox abandonne l’interface Australis au profit d’une nouvelle interface nommée Photon et se base sur le nouveau moteur de rendu Quantum. La nouvelle interface du navigateur inaugure de nombreux changements, notamment sur la page « Nouvel Onglet », mais également au sein des menus du navigateur, ou encore des thèmes et des options de personnalisation.','https://www.mozilla.org/firefox',43.8,'https://download.mozilla.org/?product=firefox-latest-ssl&os=win&lang=fr','https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=fr','65.0.1','5c79416900d20278460174.png','2019-03-02 10:36:41'),(2,2,2,1,'VLC Media Player','VLC media player, le célèbre lecteur multimédia open source, revient dans une nouvelle version majeure. Pour rappel, ce logiciel libre peut lire pratiquement tous les formats audio et vidéo ainsi que les flux réseau proposés pour les WebTV, le tout, sans avoir à télécharger quoi que soit d\'autre.','http://www.videolan.org/vlc/',39.9,'http://get.videolan.org/vlc/3.0.6/win32/vlc-3.0.6-win32.exe','http://get.videolan.org/vlc/3.0.6/win64/vlc-3.0.6-win64.exe','3.0.6','5c79444e211db354751600.png','2019-03-01 15:40:14'),(3,3,6,2,'Avast Antivirus Gratuit','Avast est disponible. Le plus populaire des Antivirus propose désormais un gestionnaire de mots de passe permettant de gérer les nombreux couples identifiants/mots de passe à partir d’une seule et unique interface. Le stockage local de ses données est chiffré, et une option permet de les sauvegarder sur les serveurs d’Avast.','https://www.avast.com/fr-fr/index#pc',0.216,'https://www.01net.com/operations/avast/telecharger/installation-ff/','https://www.01net.com/operations/avast/telecharger/installation-ff/','19.2.2364','5c79454807b1d788637708.png','2019-03-01 15:44:24'),(4,4,7,2,'CCleaner','CCleaner est destiné à optimiser ainsi qu\'à nettoyer votre système. Le logiciel retire les fichiers inutilisés de vos disques durs, les raccourcis sans cible, les contrôles ActiveX, les fichiers d’aide, les entrées dans le registre, etc. afin de libérer de l’espace et améliorer le chargement de Windows.','https://www.ccleaner.com/ccleaner',18.5,'https://download.ccleaner.com/ccsetup553.exe','https://download.ccleaner.com/ccsetup553.exe','5.53','5c7945b875f84370509172.png','2019-03-01 15:46:16'),(5,5,1,1,'LibreOffice','LibreOffice propose une alternative à la suite bureautique OpenOffice.org. Cette suite intègre toutes les applications usuelles, telles qu\'un traitement de texte, un tableur, un outil de présentation, un gestionnaire de formules mathématiques, etc.','https://www.libreoffice.org',265,'https://download.documentfoundation.org/libreoffice/stable/6.2.0/win/x86/LibreOffice_6.2.0_Win_x86.msi','https://download.documentfoundation.org/libreoffice/stable/6.2.0/win/x86_64/LibreOffice_6.2.0_Win_x64.msi','6.2.0','5c794605a2c85386753263.png','2019-03-01 15:47:33'),(6,6,3,1,'GeoGebra','GeoGebra est un logiciel éducatif en mathématiques pour les élèves du collège : algèbre, géométrie et calcul analytique. Simple et intuitive, l\'interface est divisée en deux parties : les figures géométriques et les équations correspondantes. En effet, vous pouvez réaliser vos figures en disposant directement les points ou en saisissant leurs coordonnées. Il en sera de même pour les équations, vecteurs, directions, calculs analytiques, pentes, etc.','https://www.geogebra.org',61.5,'https://download.geogebra.org/package/win-autoupdate','https://download.geogebra.org/package/win-autoupdate','6.0.523.0','5c795cee03a8f316757516.png','2019-03-01 17:25:18'),(7,7,5,1,'Brackets','Brackets propose un éditeur de code léger, libre et développé par Adobe. L\'application se veut être le parfait compagnon des développeurs et designers Web qui travaillent avec les langages HTML, CSS et JavaScript.','http://brackets.io/',72.8,'https://github.com/adobe/brackets/releases/download/release-1.13/Brackets.Release.1.13.msi','https://github.com/adobe/brackets/releases/download/release-1.13/Brackets.Release.1.13.msi','1.13','5c795423c187f485642425.png','2019-03-01 16:47:47');
/*!40000 ALTER TABLE `logiciel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logiciel_licence`
--

DROP TABLE IF EXISTS `logiciel_licence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logiciel_licence` (
  `logiciel_id` int(11) NOT NULL,
  `licence_id` int(11) NOT NULL,
  PRIMARY KEY (`logiciel_id`,`licence_id`),
  KEY `IDX_F11415A6CA84195D` (`logiciel_id`),
  KEY `IDX_F11415A626EF07C9` (`licence_id`),
  CONSTRAINT `FK_F11415A626EF07C9` FOREIGN KEY (`licence_id`) REFERENCES `licence` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F11415A6CA84195D` FOREIGN KEY (`logiciel_id`) REFERENCES `logiciel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logiciel_licence`
--

LOCK TABLES `logiciel_licence` WRITE;
/*!40000 ALTER TABLE `logiciel_licence` DISABLE KEYS */;
INSERT INTO `logiciel_licence` VALUES (1,1),(2,2),(3,3),(4,3),(5,4),(6,5),(6,6),(7,7);
/*!40000 ALTER TABLE `logiciel_licence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190301082858','2019-03-01 08:29:04'),('20190301093243','2019-03-01 09:32:49'),('20190301093424','2019-03-01 09:34:31'),('20190301093646','2019-03-01 09:36:50'),('20190301095648','2019-03-01 09:57:08'),('20190301100111','2019-03-01 10:01:16'),('20190301100824','2019-03-01 10:08:29'),('20190301101153','2019-03-01 10:11:57'),('20190301104603','2019-03-01 10:46:07'),('20190301105914','2019-03-01 10:59:17'),('20190301113329','2019-03-01 11:33:33'),('20190301130458','2019-03-01 13:05:02'),('20190301130607','2019-03-01 13:06:18'),('20190301133859','2019-03-01 13:39:05'),('20190301134002','2019-03-01 13:40:09'),('20190301134053','2019-03-01 13:40:58');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@windstore.fr','[\"ROLE_ADMIN\"]','$argon2i$v=19$m=1024,t=2,p=2$OGdzMWVyVVloZzFWbTUySA$MB41LAiuzh9bVgYsLD0ZEU17DQPJk94m6PjLIMAvmSQ');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-02 13:51:58
