<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogicielRepository")
 * @Vich\Uploadable
 * @ApiResource
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "nom" : "exact", "description": "partial", "distribution" : "exact", "categorie" : "exact", "licence" : "exact"})
 */
class Logiciel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Developpeur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $developpeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siteWeb;

    /**
     * @ORM\Column(type="float")
     */
    private $taille;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lienDL32;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lienDL64;

    /**
     * @ORM\Column(type="string")
     */
    private $version;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @Vich\UploadableField(mapping="logo_logiciel", fileNameProperty="logoName")
     * @var File|null
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $logoName;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Distribution")
     * @ORM\JoinColumn(nullable=false)
     */
    private $distribution;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Licence")
     * @ORM\JoinColumn(nullable=false)
     */
    private $licence;

    /**
     * Logiciel constructor.
     */
    public function __construct()
    {
        $this->licence = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        if ($nom) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        if ($description) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getDeveloppeur(): ?Developpeur
    {
        return $this->developpeur;
    }

    public function setDeveloppeur(?Developpeur $developpeur): self
    {
        $this->developpeur = $developpeur;
        if ($developpeur) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getSiteWeb(): ?string
    {
        return $this->siteWeb;
    }

    public function setSiteWeb(?string $siteWeb): self
    {
        $this->siteWeb = $siteWeb;
        if ($siteWeb) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getTaille(): ?float
    {
        return $this->taille;
    }

    public function setTaille(float $taille): self
    {
        $this->taille = $taille;
        if ($taille) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getLienDL32(): ?string
    {
        return $this->lienDL32;
    }

    public function setLienDL32(string $lienDL32): self
    {
        $this->lienDL32 = $lienDL32;
        if ($lienDL32) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getLienDL64(): ?string
    {
        return $this->lienDL64;
    }

    public function setLienDL64(string $lienDL64): self
    {
        $this->lienDL64 = $lienDL64;
        if ($lienDL64) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
        if ($version) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;
        if ($categorie) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    /**
     * @param File|null $logoFile
     * @throws \Exception
     */
    public function setLogoFile(File $logoFile = null): void
    {
        $this->logoFile = $logoFile;
        if ($logoFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return string|null
     */
    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    /**
     * @param string|null $logoName
     */
    public function setLogoName(string $logoName = null): void
    {
        $this->logoName = $logoName;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getDistribution(): ?Distribution
    {
        return $this->distribution;
    }

    public function setDistribution(?Distribution $distribution): self
    {
        $this->distribution = $distribution;
        if ($distribution) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return Collection|Licence[]
     */
    public function getLicence(): Collection
    {
        return $this->licence;
    }

    public function addLicence(Licence $licence): self
    {
        if (!$this->licence->contains($licence)) {
            $this->licence[] = $licence;
        }

        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    public function removeLicence(Licence $licence): self
    {
        if ($this->licence->contains($licence)) {
            $this->licence->removeElement($licence);
        }

        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }
}
