<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array('label'=>'Nom', 'attr' => array('placeholder' => 'ex : Louis Durand'), 'constraints' => array(new NotBlank(array("message" => "Merci de renseigner votre nom.")))))
            ->add('email', EmailType::class, array('label'=>'Email','attr'=> array('placeholder'=>'ex : louis.durand@domaine.fr'), 'constraints' => array(new NotBlank(array("message" => "Merci de renseigner votre email.")))))
            ->add('message',TextareaType::class,array('label'=>'Message','attr'=> array('placeholder'=>'Votre message...'), 'constraints' => array(new NotBlank(array("message" => "Merci de renseigner votre message.")))))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}