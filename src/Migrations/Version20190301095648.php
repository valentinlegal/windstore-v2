<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301095648 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE distribution (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE licence (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE software (id INT AUTO_INCREMENT NOT NULL, developpeur_id INT NOT NULL, categorie_id INT NOT NULL, distribution_id INT NOT NULL, licence_id INT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, site_web VARCHAR(255) DEFAULT NULL, taille DOUBLE PRECISION NOT NULL, lien_dl32 VARCHAR(255) NOT NULL, lien_dl64 VARCHAR(255) NOT NULL, version DOUBLE PRECISION NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_77D068CF84E66085 (developpeur_id), INDEX IDX_77D068CFBCF5E72D (categorie_id), INDEX IDX_77D068CF6EB6DDB5 (distribution_id), INDEX IDX_77D068CF26EF07C9 (licence_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE software ADD CONSTRAINT FK_77D068CF84E66085 FOREIGN KEY (developpeur_id) REFERENCES developpeur (id)');
        $this->addSql('ALTER TABLE software ADD CONSTRAINT FK_77D068CFBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE software ADD CONSTRAINT FK_77D068CF6EB6DDB5 FOREIGN KEY (distribution_id) REFERENCES distribution (id)');
        $this->addSql('ALTER TABLE software ADD CONSTRAINT FK_77D068CF26EF07C9 FOREIGN KEY (licence_id) REFERENCES licence (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE software DROP FOREIGN KEY FK_77D068CF6EB6DDB5');
        $this->addSql('ALTER TABLE software DROP FOREIGN KEY FK_77D068CF26EF07C9');
        $this->addSql('DROP TABLE distribution');
        $this->addSql('DROP TABLE licence');
        $this->addSql('DROP TABLE software');
    }
}
